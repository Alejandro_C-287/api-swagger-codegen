package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.buzzpoints.api.consumer.model.Condition;
import com.buzzpoints.api.consumer.model.MerchantLocation;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* EarnOpportunity
*/

@Data
@Builder
public class EarnOpportunity {

    /* Unique identifier for the earning oppurtunity */
    @NotBlank(message = "id cannot be null or whitespace") 
    private String id;

    /* The earning type behavior, tenant, merchant */
    @NotNull(message = "type cannot be null")
    private TypeEnum type;

    /* The status of the transaction */
    @Builder.Default
    private StatusEnum status = null;

    /* URL to the logo associated with the earning opportunity */
    @Builder.Default
    private String logo = null;

    /* The title associated to the earning opportunity */
    @NotBlank(message = "title cannot be null or whitespace") 
    private String title;

    /* The description associated to the earning opportunity */
    @Builder.Default
    private String description = null;

    /* The end date of the earning opportunity using format yyyy-mm-dd */
    @Builder.Default
    private String endDate = null;

    /* Bonus points earnt for the earning opportunity */
    @Builder.Default
    private Integer bonusPoints = null;

    /* The multiplier used to calculate the points */
    @Builder.Default
    private BigDecimal multiplier = null;

    /* Conditions associated with a transaction */
    @Builder.Default
    private java.util.List<Condition> conditions = new java.util.ArrayList<>();

    /* Locations where the earn oppurtunity can be made */
    @Builder.Default
    private java.util.List<MerchantLocation> locations = new java.util.ArrayList<>();


    /**
    * The earning type behavior, tenant, merchant
    */
    public enum TypeEnum {
        TENANT_TRANSACTION,
        BEHAVIOR_ACCOUNT_OPEN,
        BEHAVIOR_ACCOUNT_MIN_BAL,
        BEHAVIOR_ACCOUNT_ESTATEMENT,
        MERCHANT;
        
    }

    /**
    * The status of the transaction
    */
    public enum StatusEnum {
        ACTIVE,
        UNFULFILLED;
        
    }

    public Option<StatusEnum> getStatus(){return Option.of(this.status);}
    public Option<String> getLogo(){return Option.of(this.logo);}
    public Option<String> getDescription(){return Option.of(this.description);}
    public Option<String> getEndDate(){return Option.of(this.endDate);}
    public Option<Integer> getBonusPoints(){return Option.of(this.bonusPoints);}
    public Option<BigDecimal> getMultiplier(){return Option.of(this.multiplier);}
    public Option<java.util.List<Condition>> getConditions(){return Option.of(this.conditions);}
    public Option<java.util.List<MerchantLocation>> getLocations(){return Option.of(this.locations);}
}
