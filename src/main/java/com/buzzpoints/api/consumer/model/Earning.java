package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.buzzpoints.api.consumer.model.Amount;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Earning
*/

@Data
@Builder
public class Earning {

    /* Different type of earning supported by the platform e.g. cash back, gift card, travel, charity */
    @Builder.Default
    private EarningTypeEnum earningType = null;

    
    @Builder.Default
    private Amount amount = null;


    /**
    * Different type of earning supported by the platform e.g. cash back, gift card, travel, charity
    */
    public enum EarningTypeEnum {
        CASHBACK,
        GIFTCARD,
        TRAVEL,
        CHARITY;
        
    }

    public Option<EarningTypeEnum> getEarningType(){return Option.of(this.earningType);}
    public Option<Amount> getAmount(){return Option.of(this.amount);}
}
