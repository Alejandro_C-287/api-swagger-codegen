package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.buzzpoints.api.consumer.model.Condition;
import com.buzzpoints.api.consumer.model.Reward;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* FinancialReward
*/

@Data
@Builder
public class FinancialReward {

    /* Unique identifier for a reward */
    @NotBlank(message = "id cannot be null or whitespace") 
    private String id;

    /* The title associated with the reward */
    @Builder.Default
    private String title = null;

    /* The description associated with the reward */
    @Builder.Default
    private String description = null;

    /* The points value for the reward */
    @Builder.Default
    private Integer pointsValue = null;

    /* Whether the reward has been discounted */
    @Builder.Default
    private Boolean isDiscounted = null;

    /* The discount value associated with the reward */
    @Builder.Default
    private Integer discountPointsValue = null;

    /* The monetary benefit associated with the reward */
    @Builder.Default
    private Integer monetaryBenefit = null;

    /* A list of conditions associated with the reward */
    @Builder.Default
    private java.util.List<Condition> conditions = new java.util.ArrayList<>();

    /* Something */
    @Builder.Default
    private BigDecimal multiplier = null;

    /* How the financial reward is to be fulfilled */
    @NotBlank(message = "fulfilment cannot be null or whitespace") 
    private String fulfilment;


    public Option<String> getTitle(){return Option.of(this.title);}
    public Option<String> getDescription(){return Option.of(this.description);}
    public Option<Integer> getPointsValue(){return Option.of(this.pointsValue);}
    public Option<Boolean> isIsDiscounted(){return Option.of(this.isDiscounted);}
    public Option<Integer> getDiscountPointsValue(){return Option.of(this.discountPointsValue);}
    public Option<Integer> getMonetaryBenefit(){return Option.of(this.monetaryBenefit);}
    public Option<java.util.List<Condition>> getConditions(){return Option.of(this.conditions);}
    public Option<BigDecimal> getMultiplier(){return Option.of(this.multiplier);}
}
