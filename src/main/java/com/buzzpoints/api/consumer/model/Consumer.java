package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.buzzpoints.api.consumer.model.Location;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Consumer
*/

@Data
@Builder
public class Consumer {

    /* Unique identifier for the consumer */
    @NotBlank(message = "id cannot be null or whitespace") 
    private String id;

    /* The name of the consumer */
    @NotBlank(message = "name cannot be null or whitespace") 
    private String name;

    /* The home phone number of the consumer */
    @Builder.Default
    private String homePhone = null;

    /* The mobile phone number of the consumer */
    @Builder.Default
    private String mobilePhone = null;

    /* The local date time that the consumer was created */
    @Builder.Default
    private String created = null;

    /* The email of the consumer */
    @Builder.Default
    private String email = null;

    /* The password associated with the consumers account */
    @Builder.Default
    private String password = null;

    /* The points balance of the consumer */
    @Builder.Default
    private Integer pointBalance = null;

    /* The consumer&#39;s tenants account number. */
    @Builder.Default
    private String accountNumber = null;

    /* The consumer&#39;s last four social security number digits */
    @Builder.Default
    private String lastFourSSN = null;

    /* Affirms that the consumer has read and accepted the terms and conditions. */
    @Builder.Default
    private String termsAccepted = null;

    /* Locations associated with a consumer */
    @Builder.Default
    private java.util.List<Location> locations = new java.util.ArrayList<>();


    public Option<String> getHomePhone(){return Option.of(this.homePhone);}
    public Option<String> getMobilePhone(){return Option.of(this.mobilePhone);}
    public Option<String> getCreated(){return Option.of(this.created);}
    public Option<String> getEmail(){return Option.of(this.email);}
    public Option<String> getPassword(){return Option.of(this.password);}
    public Option<Integer> getPointBalance(){return Option.of(this.pointBalance);}
    public Option<String> getAccountNumber(){return Option.of(this.accountNumber);}
    public Option<String> getLastFourSSN(){return Option.of(this.lastFourSSN);}
    public Option<String> getTermsAccepted(){return Option.of(this.termsAccepted);}
    public Option<java.util.List<Location>> getLocations(){return Option.of(this.locations);}
}
