package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* ErrorMessage
*/

@Data
@Builder
public class ErrorMessage {

    /* HTPP status code */
    @Builder.Default
    private String status = null;

    /* Details of the error message */
    @Builder.Default
    private String detail = null;

    /* Optional error code */
    @Builder.Default
    private String code = null;

    /* Optional link to more information on the error */
    @Builder.Default
    private String link = null;


    public Option<String> getStatus(){return Option.of(this.status);}
    public Option<String> getDetail(){return Option.of(this.detail);}
    public Option<String> getCode(){return Option.of(this.code);}
    public Option<String> getLink(){return Option.of(this.link);}
}
