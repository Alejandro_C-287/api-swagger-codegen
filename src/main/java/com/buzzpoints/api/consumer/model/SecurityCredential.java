package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* SecurityCredential
*/

@Data
@Builder
public class SecurityCredential {

    /* Unique identifier for the consumer */
    @Builder.Default
    private AuthorizationTypeEnum authorizationType = null;

    /* value to authorize for the consumer */
    @Builder.Default
    private String authorizationValue = null;


    /**
    * Unique identifier for the consumer
    */
    public enum AuthorizationTypeEnum {
        PASSWORD,
        TOKEN,
        PIN;
        
    }

    public Option<AuthorizationTypeEnum> getAuthorizationType(){return Option.of(this.authorizationType);}
    public Option<String> getAuthorizationValue(){return Option.of(this.authorizationValue);}
}
