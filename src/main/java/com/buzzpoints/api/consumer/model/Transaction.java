package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.buzzpoints.api.consumer.model.Amount;
import com.buzzpoints.api.consumer.model.Condition;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Transaction
*/

@Data
@Builder
public class Transaction {

    /* Unique identifier for a transaction */
    @NotBlank(message = "id cannot be null or whitespace") 
    private String id;

    /* The transaction type behavior, tenant, redemption */
    @NotNull(message = "type cannot be null")
    private TypeEnum type;

    /* The status of the transaction */
    @Builder.Default
    private StatusEnum status = null;

    
    @Builder.Default
    private Amount amount = null;

    /* The title associated to the transaction */
    @NotBlank(message = "title cannot be null or whitespace") 
    private String title;

    /* The description associated to the transaction */
    @Builder.Default
    private String description = null;

    /* The date of the transaction using format yyyy-mm-dd */
    @Builder.Default
    private String date = null;

    /* Points missed if the transaction had been associated with an account */
    @Builder.Default
    private Integer missedPoints = null;

    /* Bonus points earnt due to the transaction */
    @Builder.Default
    private Integer bonusPoints = null;

    /* Points earnt for transaction */
    @Builder.Default
    private Integer points = null;

    /* The multiplier used to calculate the points */
    @Builder.Default
    private BigDecimal multiplier = null;

    /* Conditions associated with a transaction */
    @Builder.Default
    private java.util.List<Condition> conditions = new java.util.ArrayList<>();


    /**
    * The transaction type behavior, tenant, redemption
    */
    public enum TypeEnum {
        TENANT_TRANSACTION,
        BEHAVIOR_ACCOUNT_OPEN,
        BEHAVIOR_ACCOUNT_MIN_BAL,
        BEHAVIOR_ACCOUNT_ESTATEMENT,
        REDEMPTION_RTR,
        REDEMPTION_GIFTCARD,
        REDEMPTION_CASHBACK,
        REDEMPTION_CHARITY,
        REDEMPTION_FINANCIAL,
        REDEMPTION_TRAVEL;
        
    }

    /**
    * The status of the transaction
    */
    public enum StatusEnum {
        FULFILLED,
        UNFULFILLED;
        
    }

    public Option<StatusEnum> getStatus(){return Option.of(this.status);}
    public Option<Amount> getAmount(){return Option.of(this.amount);}
    public Option<String> getDescription(){return Option.of(this.description);}
    public Option<String> getDate(){return Option.of(this.date);}
    public Option<Integer> getMissedPoints(){return Option.of(this.missedPoints);}
    public Option<Integer> getBonusPoints(){return Option.of(this.bonusPoints);}
    public Option<Integer> getPoints(){return Option.of(this.points);}
    public Option<BigDecimal> getMultiplier(){return Option.of(this.multiplier);}
    public Option<java.util.List<Condition>> getConditions(){return Option.of(this.conditions);}
}
