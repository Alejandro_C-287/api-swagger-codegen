package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Account
*/

@Data
@Builder
public class Account {

    /* Unique identifier for an account */
    @Builder.Default
    private String id = null;

    /* The name of the account */
    @Builder.Default
    private String name = null;

    /* Account type can one of the following debit, credit, checking, saving, loan, insurance */
    @Builder.Default
    private AccountTypeEnum accountType = null;

    /* The account number restricted to last 4 if part of return from Buzz Points */
    @Builder.Default
    private String accountNumber = null;


    /**
    * Account type can one of the following debit, credit, checking, saving, loan, insurance
    */
    public enum AccountTypeEnum {
        CHECKING,
        SAVING,
        LOAN,
        INSURANCE;
        
    }

    public Option<String> getId(){return Option.of(this.id);}
    public Option<String> getName(){return Option.of(this.name);}
    public Option<AccountTypeEnum> getAccountType(){return Option.of(this.accountType);}
    public Option<String> getAccountNumber(){return Option.of(this.accountNumber);}
}
