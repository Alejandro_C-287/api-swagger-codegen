package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Location
*/

@Data
@Builder
public class Location {

    /* Unique identifier for a location */
    @Builder.Default
    private String id = null;

    /* Location type can one of the following shipping, current, home */
    @Builder.Default
    private LocationTypeEnum locationType = null;

    /* The street address for the location */
    @Builder.Default
    private String street = null;

    /* The city associated with the location */
    @Builder.Default
    private String city = null;

    /* The state associated with the location */
    @Builder.Default
    private String state = null;

    /* The zipCode associated with the location */
    @Builder.Default
    private String zipCode = null;

    /* The latitude associated with the location */
    @Builder.Default
    private String latitude = null;

    /* The longitude associated with the location */
    @Builder.Default
    private String longitude = null;

    /* The local date time that the location was updated */
    @Builder.Default
    private String updated = null;


    /**
    * Location type can one of the following shipping, current, home
    */
    public enum LocationTypeEnum {
        SHIPPING,
        CURRENT,
        HOME;
        
    }

    public Option<String> getId(){return Option.of(this.id);}
    public Option<LocationTypeEnum> getLocationType(){return Option.of(this.locationType);}
    public Option<String> getStreet(){return Option.of(this.street);}
    public Option<String> getCity(){return Option.of(this.city);}
    public Option<String> getState(){return Option.of(this.state);}
    public Option<String> getZipCode(){return Option.of(this.zipCode);}
    public Option<String> getLatitude(){return Option.of(this.latitude);}
    public Option<String> getLongitude(){return Option.of(this.longitude);}
    public Option<String> getUpdated(){return Option.of(this.updated);}
}
