package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Amount
*/

@Data
@Builder
public class Amount {

    /* ISO 4217 currency code for the earning amount */
    @Builder.Default
    private String isoCurrencyCode = null;

    /* total earning amount for the earning type */
    @Builder.Default
    private String amount = null;


    public Option<String> getIsoCurrencyCode(){return Option.of(this.isoCurrencyCode);}
    public Option<String> getAmount(){return Option.of(this.amount);}
}
