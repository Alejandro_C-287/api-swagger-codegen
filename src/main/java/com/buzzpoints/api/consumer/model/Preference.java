package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Preference
*/

@Data
@Builder
public class Preference {

    /* Key to identify a preference value */
    @Builder.Default
    private String key = null;

    /* Value of associated with the preference key */
    @Builder.Default
    private String value = null;


    public Option<String> getKey(){return Option.of(this.key);}
    public Option<String> getValue(){return Option.of(this.value);}
}
