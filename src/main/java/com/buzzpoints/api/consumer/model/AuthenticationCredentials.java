package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* AuthenticationCredentials
*/

@Data
@Builder
public class AuthenticationCredentials {

    
    @Builder.Default
    private String oldPassword = null;

    
    @Builder.Default
    private String newPassword = null;


    public Option<String> getOldPassword(){return Option.of(this.oldPassword);}
    public Option<String> getNewPassword(){return Option.of(this.newPassword);}
}
