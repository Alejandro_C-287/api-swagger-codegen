package com.buzzpoints.api.consumer.model;

import java.util.Objects;
import java.util.Arrays;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import lombok.Data;
import lombok.Builder;
import io.vavr.control.Option;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* Card
*/

@Data
@Builder
public class Card {

    /* Unique identifier for the card */
    @Builder.Default
    private String id = null;

    /* The name of the card */
    @Builder.Default
    private String name = null;

    /* Card type can one of the following debit, credit, prepaid */
    @Builder.Default
    private CardTypeEnum cardType = null;

    /* Scheme associated with the card */
    @Builder.Default
    private SchemeEnum scheme = null;

    /* The pan number restricted to last 4 if part of return from Buzz Points */
    @Builder.Default
    private String panNumber = null;

    /* The CCV number never returned by Buzz Points */
    @Builder.Default
    private String cvv = null;

    /* Expiry date of card in format mm-yyyy */
    @Builder.Default
    private String expiryDate = null;


    /**
    * Card type can one of the following debit, credit, prepaid
    */
    public enum CardTypeEnum {
        DEBIT,
        CREDIT,
        PREPAID;
        
    }

    /**
    * Scheme associated with the card
    */
    public enum SchemeEnum {
        VISA,
        MASTERCARD;
        
    }

    public Option<String> getId(){return Option.of(this.id);}
    public Option<String> getName(){return Option.of(this.name);}
    public Option<CardTypeEnum> getCardType(){return Option.of(this.cardType);}
    public Option<SchemeEnum> getScheme(){return Option.of(this.scheme);}
    public Option<String> getPanNumber(){return Option.of(this.panNumber);}
    public Option<String> getCvv(){return Option.of(this.cvv);}
    public Option<String> getExpiryDate(){return Option.of(this.expiryDate);}
}
