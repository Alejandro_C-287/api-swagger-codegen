/**
* NOTE: This class is auto generated by the swagger code generator program (2.4.5-SNAPSHOT).
* https://github.com/swagger-api/swagger-codegen
* Do not edit the class manually.
*/
package com.buzzpoints.api.consumer.controller;

import com.buzzpoints.api.consumer.model.ErrorMessage;
import com.buzzpoints.api.consumer.model.Reward;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@RequestMapping(path = "https://api.buzzpoints.com/{tenantId}")
public interface BurnsApi {

    @RequestMapping(value = "/burns/{consumerId}/{rewardId}", method = RequestMethod.POST, 
        produces = { "application/json" }, consumes = { "application/json" })
    @ResponseBody
    Mono<Reward> burnsConsumerIdRewardIdPost(@PathVariable("consumerId") String consumerId,@PathVariable("rewardId") String rewardId);

    @RequestMapping(value = "/burns/", method = RequestMethod.GET, 
        produces = { "application/json" }, consumes = { "application/json" })
    @ResponseBody
    Flux<Reward> burnsGet(@NotNull @Valid @RequestParam(value = "latitude") String latitude,@NotNull @Valid @RequestParam(value = "longitude") String longitude,@NotNull @Valid @RequestParam(value = "limit") Integer limit,@Valid @RequestParam(value = "burnType", required = false) String burnType,@Valid @RequestParam(value = "page", required = false) Integer page);

}
