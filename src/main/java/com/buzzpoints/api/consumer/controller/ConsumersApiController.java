package com.buzzpoints.api.consumer.controller;

import com.buzzpoints.api.consumer.model.Account;
import com.buzzpoints.api.consumer.model.AuthenticationCredentials;
import com.buzzpoints.api.consumer.model.Balance;
import com.buzzpoints.api.consumer.model.Card;
import com.buzzpoints.api.consumer.model.Consumer;
import com.buzzpoints.api.consumer.model.EarnOpportunity;
import com.buzzpoints.api.consumer.model.Earning;
import com.buzzpoints.api.consumer.model.ErrorMessage;
import com.buzzpoints.api.consumer.model.Preference;
import com.buzzpoints.api.consumer.model.Preference1;
import com.buzzpoints.api.consumer.model.Preference2;
import com.buzzpoints.api.consumer.model.SecurityCredential;
import com.buzzpoints.api.consumer.model.Transaction;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Optional;
import java.io.IOException;
import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ConsumersApiController implements ConsumersApi {
    private final ConsumersApiDelegate delegate;

    @Autowired
    public ConsumersApiController(ConsumersApiDelegate delegate) {
        this.delegate = delegate;
    }

    public Mono<Void> consumersConsumerIdAccountsAccountIdDelete(String consumerId,String accountId) {
        return delegate.consumersConsumerIdAccountsAccountIdDelete(consumerId, accountId);
    }

    public Flux<Account> consumersConsumerIdAccountsGet(String consumerId) {
        return delegate.consumersConsumerIdAccountsGet(consumerId);
    }

    public Mono<Void> consumersConsumerIdAccountsPost(String consumerId,Account account) {
        return delegate.consumersConsumerIdAccountsPost(consumerId, account);
    }

    public Mono<Void> consumersConsumerIdAccountsPut(String consumerId,Account account) {
        return delegate.consumersConsumerIdAccountsPut(consumerId, account);
    }

    public Mono<Void> consumersConsumerIdAuthorizationPost(String consumerId,SecurityCredential body) {
        return delegate.consumersConsumerIdAuthorizationPost(consumerId, body);
    }

    public Flux<Balance> consumersConsumerIdBalancesGet(String consumerId) {
        return delegate.consumersConsumerIdBalancesGet(consumerId);
    }

    public Mono<Void> consumersConsumerIdCardsCardIdDelete(String consumerId,String cardId) {
        return delegate.consumersConsumerIdCardsCardIdDelete(consumerId, cardId);
    }

    public Flux<Account> consumersConsumerIdCardsGet(String consumerId) {
        return delegate.consumersConsumerIdCardsGet(consumerId);
    }

    public Mono<Void> consumersConsumerIdCardsPost(String consumerId,Card card) {
        return delegate.consumersConsumerIdCardsPost(consumerId, card);
    }

    public Mono<Void> consumersConsumerIdCardsPut(String consumerId,Card card) {
        return delegate.consumersConsumerIdCardsPut(consumerId, card);
    }

    public Mono<Earning> consumersConsumerIdEarningsEarningTypeGet(String consumerId,String earningType) {
        return delegate.consumersConsumerIdEarningsEarningTypeGet(consumerId, earningType);
    }

    public Flux<Earning> consumersConsumerIdEarningsGet(String consumerId) {
        return delegate.consumersConsumerIdEarningsGet(consumerId);
    }

    public Flux<EarnOpportunity> consumersConsumerIdEarnsGet(String consumerId, String latitude, String longitude, Integer limit, String earnType, Integer page) {
        return delegate.consumersConsumerIdEarnsGet(consumerId, latitude, longitude, limit, earnType, page);
    }

    public Mono<Consumer> consumersConsumerIdGet(String consumerId) {
        return delegate.consumersConsumerIdGet(consumerId);
    }

    public Mono<Void> consumersConsumerIdPasswordPut(String consumerId,AuthenticationCredentials authenticationCredentials) {
        return delegate.consumersConsumerIdPasswordPut(consumerId, authenticationCredentials);
    }

    public Flux<Preference> consumersConsumerIdPreferencesGet(String consumerId) {
        return delegate.consumersConsumerIdPreferencesGet(consumerId);
    }

    public Mono<Void> consumersConsumerIdPreferencesPost(String consumerId,java.util.List<Preference> preferences) {
        return delegate.consumersConsumerIdPreferencesPost(consumerId, preferences);
    }

    public Mono<Void> consumersConsumerIdPreferencesPreferenceKeyDelete(String consumerId,String preferenceKey,Preference2 preference) {
        return delegate.consumersConsumerIdPreferencesPreferenceKeyDelete(consumerId, preferenceKey, preference);
    }

    public Mono<Void> consumersConsumerIdPreferencesPreferenceKeyPut(String consumerId,String preferenceKey,Preference1 preference) {
        return delegate.consumersConsumerIdPreferencesPreferenceKeyPut(consumerId, preferenceKey, preference);
    }

    public Mono<Void> consumersConsumerIdPut(String consumerId,Consumer body) {
        return delegate.consumersConsumerIdPut(consumerId, body);
    }

    public Flux<Transaction> consumersConsumerIdTransactionsGet(String consumerId, Integer limit, String transactionType, String transactionStatus, Integer page) {
        return delegate.consumersConsumerIdTransactionsGet(consumerId, limit, transactionType, transactionStatus, page);
    }

    public Mono<Transaction> consumersConsumerIdTransactionsTransactionIdGet(String consumerId,String transactionId) {
        return delegate.consumersConsumerIdTransactionsTransactionIdGet(consumerId, transactionId);
    }

    public Mono<Void> consumersPost(Consumer body) {
        return delegate.consumersPost(body);
    }

}
