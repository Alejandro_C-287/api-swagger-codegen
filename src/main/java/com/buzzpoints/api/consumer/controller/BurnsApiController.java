package com.buzzpoints.api.consumer.controller;

import com.buzzpoints.api.consumer.model.ErrorMessage;
import com.buzzpoints.api.consumer.model.Reward;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Optional;
import java.io.IOException;
import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class BurnsApiController implements BurnsApi {
    private final BurnsApiDelegate delegate;

    @Autowired
    public BurnsApiController(BurnsApiDelegate delegate) {
        this.delegate = delegate;
    }

    public Mono<Reward> burnsConsumerIdRewardIdPost(String consumerId,String rewardId) {
        return delegate.burnsConsumerIdRewardIdPost(consumerId, rewardId);
    }

    public Flux<Reward> burnsGet( String latitude, String longitude, Integer limit, String burnType, Integer page) {
        return delegate.burnsGet(latitude, longitude, limit, burnType, page);
    }

}
