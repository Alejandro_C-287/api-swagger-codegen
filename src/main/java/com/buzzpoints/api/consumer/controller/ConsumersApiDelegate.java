package com.buzzpoints.api.consumer.controller;

import com.buzzpoints.api.consumer.model.Account;
import com.buzzpoints.api.consumer.model.AuthenticationCredentials;
import com.buzzpoints.api.consumer.model.Balance;
import com.buzzpoints.api.consumer.model.Card;
import com.buzzpoints.api.consumer.model.Consumer;
import com.buzzpoints.api.consumer.model.EarnOpportunity;
import com.buzzpoints.api.consumer.model.Earning;
import com.buzzpoints.api.consumer.model.ErrorMessage;
import com.buzzpoints.api.consumer.model.Preference;
import com.buzzpoints.api.consumer.model.Preference1;
import com.buzzpoints.api.consumer.model.Preference2;
import com.buzzpoints.api.consumer.model.SecurityCredential;
import com.buzzpoints.api.consumer.model.Transaction;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Optional;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ConsumersApiDelegate {
    Mono<Void> consumersConsumerIdAccountsAccountIdDelete(String consumerId,String accountId);
    Flux<Account> consumersConsumerIdAccountsGet(String consumerId);
    Mono<Void> consumersConsumerIdAccountsPost(String consumerId,Account account);
    Mono<Void> consumersConsumerIdAccountsPut(String consumerId,Account account);
    Mono<Void> consumersConsumerIdAuthorizationPost(String consumerId,SecurityCredential body);
    Flux<Balance> consumersConsumerIdBalancesGet(String consumerId);
    Mono<Void> consumersConsumerIdCardsCardIdDelete(String consumerId,String cardId);
    Flux<Account> consumersConsumerIdCardsGet(String consumerId);
    Mono<Void> consumersConsumerIdCardsPost(String consumerId,Card card);
    Mono<Void> consumersConsumerIdCardsPut(String consumerId,Card card);
    Mono<Earning> consumersConsumerIdEarningsEarningTypeGet(String consumerId,String earningType);
    Flux<Earning> consumersConsumerIdEarningsGet(String consumerId);
    Flux<EarnOpportunity> consumersConsumerIdEarnsGet(String consumerId, String latitude, String longitude, Integer limit, String earnType, Integer page);
    Mono<Consumer> consumersConsumerIdGet(String consumerId);
    Mono<Void> consumersConsumerIdPasswordPut(String consumerId,AuthenticationCredentials authenticationCredentials);
    Flux<Preference> consumersConsumerIdPreferencesGet(String consumerId);
    Mono<Void> consumersConsumerIdPreferencesPost(String consumerId,java.util.List<Preference> preferences);
    Mono<Void> consumersConsumerIdPreferencesPreferenceKeyDelete(String consumerId,String preferenceKey,Preference2 preference);
    Mono<Void> consumersConsumerIdPreferencesPreferenceKeyPut(String consumerId,String preferenceKey,Preference1 preference);
    Mono<Void> consumersConsumerIdPut(String consumerId,Consumer body);
    Flux<Transaction> consumersConsumerIdTransactionsGet(String consumerId, Integer limit, String transactionType, String transactionStatus, Integer page);
    Mono<Transaction> consumersConsumerIdTransactionsTransactionIdGet(String consumerId,String transactionId);
    Mono<Void> consumersPost(Consumer body);
}
